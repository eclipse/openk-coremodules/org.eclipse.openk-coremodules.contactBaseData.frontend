/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class ModifiedContacts {
  public searchText: string = null;
  public contactTypeId: string = '';
  public sort: string = null;
  public isDSGVOFilterAdvancedVisible: boolean = null;
  public moduleName: string = null;
  public withoutAssignedModule: boolean = false;
  public expiringDataInPast: boolean = false;
  public deletionLockExceeded: boolean = false;
  public withSyncError: boolean = false;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
