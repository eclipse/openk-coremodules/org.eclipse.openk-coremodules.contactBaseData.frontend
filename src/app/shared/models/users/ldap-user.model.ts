/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class LdapUser {
  public department: string = null;
  public firstName: string = null;
  public fullName: string = null;
  public lastName: string = null;
  public mail: string = null;
  public password: string = null;
  public telephoneNumber: string = null;
  public title: string = null;
  public uid: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }

  get ldapSearchString(): string {
    return this.firstName + ' ' + this.lastName + ' (' + this.uid + ')';
  }
}
