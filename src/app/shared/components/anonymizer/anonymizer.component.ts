/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AnonymizerSandbox } from '@shared/components/anonymizer/anonymizer.sandbox';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-anonymizer',
  templateUrl: './anonymizer.component.html',
  styleUrls: ['./anonymizer.component.scss'],
})
export class AnonymizerComponent implements OnInit, OnDestroy {
  @Input() contactId: string;

  constructor(private _sandbox: AnonymizerSandbox) {}

  public anonymizeContact(): void {
    this._sandbox.anonymizeContact(this.contactId);
  }
  ngOnInit() {
    this._sandbox.init();
  }
  ngOnDestroy() {
    this._sandbox.endSubscriptions();
    this.contactId = null;
  }
}
