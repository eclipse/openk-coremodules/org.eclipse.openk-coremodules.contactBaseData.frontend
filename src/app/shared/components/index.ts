/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbDateParserFormatter, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { AnonymizerComponent } from '@shared/components/anonymizer/anonymizer.component';
import { AnonymizerSandbox } from '@shared/components/anonymizer/anonymizer.sandbox';
import { BoolCellRendererComponent } from '@shared/components/cell-renderer/bool-cell-renderer/bool-cell-renderer.component';
import { ContactTypeCellRendererComponent } from '@shared/components/cell-renderer/contact-type-cell-renderer/contact-type-cell-renderer.component';
import { DateCellRendererComponent } from '@shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { ExpandableComponent } from '@shared/components/expandable/expandable.component';
import { HeaderComponent } from '@shared/components/header/header.component';
import { UserModuleAssignmentDataDetailsComponent } from '@shared/components/list-details-view/user-module-assignment/details/details.component';
import { UserModuleAssignmentDataListComponent } from '@shared/components/list-details-view/user-module-assignment/list/list.component';
import { UserModuleAssignmentApiClient } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment-api-client';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { UserModuleAssignmentsService } from '@shared/components/list-details-view/user-module-assignment/user-module-assignments.service';
import { LoadingSpinnerComponent } from '@shared/components/loading-spinner/loading-spinner.component';
import { PageNotFoundComponent } from '@app/shared/components/page-not-found/page-not-found.component';
import { PaginatorComponent } from '@shared/components/paginator/paginator.component';
import { SpinnerComponent } from '@shared/components/spinner/spinner.component';
import { DirectivesModule } from '@shared/directives/index';
import { PipesModule } from '@shared/pipes';
import { NgbDateCustomParserFormatter } from '@shared/pipes/ngb-date-custom-parser-formatter';
import { userModuleAssignmentReducers } from '@shared/store';
import { UserModuleAssignmentEffect } from '@shared/store/effects/user-module-assignment/user-module-assignment.effect';
import { AgGridModule } from 'ag-grid-angular';
import { NgrxFormsModule } from 'ngrx-forms';
import { VersionInfo } from './version-info/version-info.component';

export const COMPONENTS = [
  SpinnerComponent,
  HeaderComponent,
  PageNotFoundComponent,
  VersionInfo,
  LoadingSpinnerComponent,
  PaginatorComponent,
  ContactTypeCellRendererComponent,
  IconCellRendererComponent,
  SafetyQueryDialogComponent,
  ExpandableComponent,
  SafetyQueryDialogComponent,
  BoolCellRendererComponent,
  UserModuleAssignmentDataDetailsComponent,
  UserModuleAssignmentDataListComponent,
  DateCellRendererComponent,
  AnonymizerComponent,
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    DirectivesModule,
    ReactiveFormsModule,
    RouterModule,
    NgrxFormsModule,
    FormsModule,
    FontAwesomeModule,
    NgbDatepickerModule,
    AgGridModule,
    StoreModule.forFeature('userModuleAssignmentData', userModuleAssignmentReducers),
    EffectsModule.forFeature([UserModuleAssignmentEffect]),
    PipesModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [
    UserModuleAssignmentSandBox,
    UserModuleAssignmentApiClient,
    UserModuleAssignmentsService,
    DatePipe,
    { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter },
    AnonymizerSandbox,
  ],
})
export class ComponentsModule {}
