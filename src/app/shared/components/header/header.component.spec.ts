/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HeaderComponent } from '@shared/components/header/header.component';
import { of } from 'rxjs';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let router: any;
  let sessionStorage: any;
  let window: any;

  beforeEach(() => {
    router = { navigateByUrl() {} } as any;
    component = new HeaderComponent(router);
    window = { location: { reload() {} } } as any;
    sessionStorage = { clear() {} } as any;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to overview after call navigateToOverview', () => {
    const spy = spyOn(router, 'navigateByUrl').and.returnValue({ then: () => of(true) });

    component.navigateToOverview();
    expect(spy).toHaveBeenCalled();
  });
});
