/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { CommunicationType } from '@shared/models';

export interface ILoadCommunicationTypesSuccess {
  payload: Array<CommunicationType>;
}
export interface ILoadCommunicationTypesFail {
  payload: string;
}

export const loadCommunicationTypes = createAction('[CommunicationTypes] Load');
export const loadCommunicationTypesSuccess = createAction(
  '[CommunicationTypes] Load Success',
  props<ILoadCommunicationTypesSuccess>());
export const loadCommunicationTypesFail = createAction(
  '[CommunicationTypes] Load Fail',
  props<ILoadCommunicationTypesFail>());

export const loadCommunicationType = createAction(
  '[CommunicationType] Load',
  props<{ payload: string }>()
);
export const loadCommunicationTypeSuccess = createAction(
  '[CommunicationType] Load Success',
  props<{ payload: CommunicationType }>()
);
export const loadCommunicationTypeFail = createAction(
  '[CommunicationType] Load Fail',
  props<{ payload: string }>()
);

export const saveCommunicationType = createAction(
  '[CommunicationType Details] Save',
  props<{ payload: CommunicationType }>()
);
export const saveCommunicationTypeSuccess = createAction(
  '[CommunicationType Details] Save Success',
  props<{ payload: CommunicationType }>()
);
export const saveCommunicationTypeFail = createAction(
  '[CommunicationType Details] Save Fail',
  props<{ payload: string }>()
);

export const deleteCommunicationType = createAction(
  '[CommunicationType Details] Delete',
  props<{ payload: string }>()
);
export const deleteCommunicationTypeSuccess = createAction(
  '[CommunicationType Details] Delete Success'
);
export const deleteCommunicationTypeFail = createAction(
  '[CommunicationType Details] Delete Fail',
  props<{ payload: string }>()
);
