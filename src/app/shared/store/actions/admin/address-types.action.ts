/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { AddressType } from '@shared/models';

export interface ILoadAddressTypesSuccess {
  payload: Array<AddressType>;
}

export interface ILoadAddressTypesFail {
  payload: string;
}

export const loadAddressTypes = createAction('[AddressTypes] Load');
export const loadAddressTypesSuccess = createAction(
  '[AddressTypes] Load Success',
  props<ILoadAddressTypesSuccess>());
export const loadAddressTypesFail = createAction(
  '[AddressTypes] Load Fail',
  props<ILoadAddressTypesFail>());

export const loadAddressType = createAction(
  '[AddressType Details] Load',
  props<{ payload: string }>()
);
export const loadAddressTypeSuccess = createAction(
  '[AddressType Details] Load Success',
  props<{ payload: AddressType }>()
);
export const loadAddressTypeFail = createAction(
  '[AddressType Details] Load Fail',
  props<{ payload: string }>()
);

export const saveAddressType = createAction(
  '[AddressType Details] Save',
  props<{ payload: AddressType }>()
);
export const saveAddressTypeSuccess = createAction(
  '[AddressType Details] Save Success',
  props<{ payload: AddressType }>()
);
export const saveAddressTypeFail = createAction(
  '[AddressType Details] Save Fail',
  props<{ payload: string }>()
);

export const deleteAddressType = createAction(
  '[AddressType Details] Delete',
  props<{ payload: string }>()
);
export const deleteAddressTypeSuccess = createAction(
  '[AddressType Details] Delete Success'
);
export const deleteAddressTypeFail = createAction(
  '[AddressType Details] Delete Fail',
  props<{ payload: string }>()
);
