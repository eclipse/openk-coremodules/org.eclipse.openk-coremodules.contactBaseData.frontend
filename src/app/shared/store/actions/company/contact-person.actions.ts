/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { CommunicationsData, ContactPerson, Address } from '@shared/models';

export interface ILoadContactPersonDetailsSuccess {
  payload: ContactPerson;
}

export interface ILoadContactPersonDetailsFail {
  payload: string;
}

export interface ILoadContactPersonAddressesSuccess {
  payload: Array<Address>;
}

export interface ILoadContactPersonAddressesFail {
  payload: string;
}

export interface ILoadContactPersonCommunicationsDataSuccess {
  payload: Array<CommunicationsData>;
}

export interface ILoadContactPersonCommunicationsDataFail {
  payload: string;
}

/* contact persons */
export const loadContactPersonDetail = createAction('[ContactPerson Details] Load', props<{ payload: string }>());
export const loadContactPersonDetailSuccess = createAction('[ContactPerson Details] Load Success', props<ILoadContactPersonDetailsSuccess>());
export const loadContactPersonDetailFail = createAction('[ContactPerson Details] Load Fail', props<ILoadContactPersonDetailsFail>());

export const saveContactPersonDetail = createAction('[ContactPerson Details] Save', props<{ payload: ContactPerson }>());
export const saveContactPersonDetailSuccess = createAction('[ContactPerson Details] Save Success', props<{ payload: ContactPerson }>());
export const saveContactPersonDetailFail = createAction('[ContactPerson Details] Save Fail', props<{ payload: string }>());

export const deleteContactPerson = createAction('[ContactPerson Details] Delete', props<{ payload: ContactPerson }>());
export const deleteContactPersonSuccess = createAction('[ContactPerson Details] Delete Success');
export const deleteContactPersonFail = createAction('[ContactPerson Details] Delete Fail', props<{ payload: string }>());

/* adresses */
export const loadContactPersonDetailAddresses = createAction('[ContactPersonAddresses] Load', props<{ payload: string }>());
export const loadContactPersonDetailAddressesSuccess = createAction('[ContactPersonAddresses] Load Success', props<ILoadContactPersonAddressesSuccess>());
export const loadContactPersonDetailAddressesFail = createAction('[ContactPersonAddresses] Load Fail', props<ILoadContactPersonAddressesFail>());

export const loadContactPersonDetailAddressDetails = createAction(
  '[ContactPersonAddresses Details] Load',
  props<{ payload_contactId: string; payload_addressId: string }>()
);
export const loadContactPersonDetailAddressDetailsSuccess = createAction('[ContactPersonAddresses Details] Load Success', props<{ payload: Address }>());
export const loadContactPersonDetailAddressDetailsFail = createAction('[ContactPersonAddresses Details] Load Fail', props<{ payload: string }>());

export const persistAddressDetail = createAction('[ContactPersonAddress Details] Persist', props<{ payload: Address }>());
export const persistAddressDetailSuccess = createAction('[ContactPersonAddress Details] Persist Success', props<{ payload: Address }>());
export const persistAddressDetailFail = createAction('[ContactPersonAddress Details] Persist Fail', props<{ payload: string }>());

export const deleteAddress = createAction('[ContactPersonAddress Details] Delete', props<{ payload: Address }>());
export const deleteAddressSuccess = createAction('[ContactPersonAddress Details] Delete Success');
export const deleteAddressFail = createAction('[ContactPersonAddress Details] Delete Fail', props<{ payload: string }>());

/* communications */
export const loadContactPersonDetailCommunicationsData = createAction('[ContactPerson CommunicationsData] Load', props<{ payload: string }>());
export const loadContactPersonDetailCommunicationsDataSuccess = createAction(
  '[ContactPerson CommunicationsData] Load Success',
  props<ILoadContactPersonCommunicationsDataSuccess>()
);
export const loadContactPersonDetailCommunicationsDataFail = createAction(
  '[ContactPerson CommunicationsData] Load Fail',
  props<ILoadContactPersonCommunicationsDataFail>()
);

export const loadContactPersonDetailCommunicationsDataDetails = createAction(
  '[ContactPerson CommunicationsData Details] Load',
  props<{ payload_contactId: string; payload_communicationsId: string }>()
);
export const loadContactPersonDetailCommunicationsDataDetailsSuccess = createAction(
  '[ContactPerson CommunicationsData Details] Load Success',
  props<{ payload: CommunicationsData }>()
);
export const loadContactPersonDetailCommunicationsDataDetailsFail = createAction(
  '[ContactPerson CommunicationsData Details] Load Fail',
  props<{ payload: string }>()
);

export const persistCommunicationsDataDetail = createAction('[ContactPerson CommunicationsData Details] Persist', props<{ payload: CommunicationsData }>());
export const persistCommunicationsDataDetailSuccess = createAction(
  '[ContactPerson CommunicationsData Details] Persist Success',
  props<{ payload: CommunicationsData }>()
);
export const persistCommunicationsDataDetailFail = createAction('[ContactPerson CommunicationsData Details] Persist Fail', props<{ payload: string }>());

export const deleteCommunicationsData = createAction('[ContactPerson CommunicationsData Details] Delete', props<{ payload: CommunicationsData }>());
export const deleteCommunicationsDataSuccess = createAction('[ContactPerson CommunicationsData Details] Delete Success');
export const deleteCommunicationsDataFail = createAction('[ContactPerson CommunicationsData Details] Delete Fail', props<{ payload: string }>());
