/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { UserModuleAssignment } from '@shared/models';
import { UserModuleType } from '@shared/models/user-module-assignment/user-module-type.model';

export interface ILoadUserModuleAssignmentSuccess {
  payload: Array<UserModuleAssignment>;
}
export interface ILoadUserModuleAssignmentFail {
  payload: string;
}

export interface ILoadUserModuleTypesSuccess {
  payload: Array<UserModuleType>;
}

export interface ILoadUserModuleTypeFail {
  payload: string;
}

export const loadUserModuleAssignments = createAction('[UserModuleAssignments] Load', props<{ payload: string }>());

export const loadUserModuleAssignmentsSuccess = createAction('[UserModuleAssignments] Load Success', props<ILoadUserModuleAssignmentSuccess>());

export const loadUserModuleAssignmentsFail = createAction('[UserModuleAssignments] Load Fail', props<ILoadUserModuleAssignmentFail>());

export const loadUserModuleAssignmentDetails = createAction('[UserModuleAssignments Details] Load', props<{ payload: UserModuleAssignment }>());

export const loadUserModuleAssignmentDetailsSuccess = createAction('[UserModuleAssignments Details] Load Success', props<{ payload: UserModuleAssignment }>());

export const loadUserModuleAssignmentDetailsFail = createAction('[UserModuleAssignments Details] Load Fail', props<{ payload: string }>());

export const persistUserModuleAssignmentDetail = createAction('[UserModuleAssignments Details] Persist', props<{ payload: UserModuleAssignment }>());

export const persistUserModuleAssignmentDetailSuccess = createAction(
  '[UserModuleAssignments Details] Persist Success',
  props<{ payload: UserModuleAssignment }>()
);

export const persistUserModuleAssignmentDetailFail = createAction('[UserModuleAssignments Details] Persist Fail', props<{ payload: string }>());

export const deleteUserModuleAssignment = createAction('[UserModuleAssignments Details] Delete', props<{ payload: UserModuleAssignment }>());
export const deleteUserModuleAssignmentSuccess = createAction('[UserModuleAssignments Details] Delete Success');
export const deleteUserModuleAssignmentFail = createAction('[UserModuleAssignments Details] Delete Fail', props<{ payload: string }>());
export const loadFilteredUserModuleTypes = createAction('[UserModuleAssignmentTypes] Load', props<{ payload: string }>());
export const loadFilteredUserModuleTypesSuccess = createAction('[UserModuleAssignmentTypes] Load Success', props<ILoadUserModuleTypesSuccess>());
export const loadFilteredUserModuleTypesFail = createAction('[UserModuleAssignmentTypes] Load Fail', props<ILoadUserModuleTypeFail>());
