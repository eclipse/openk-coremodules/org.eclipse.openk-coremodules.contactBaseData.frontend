/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as addressTypesActions from '@shared/store/actions/admin/address-types.action';
import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  updateGroup,
  FormGroupState,
  FormState,
  SetValueAction,
  validate,
} from 'ngrx-forms';
import { AddressType } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'addressTypeDetailForm';

export const INITIAL_STATE: FormGroupState<AddressType> = createFormGroupState<
  AddressType
>(FORM_ID, new AddressType());

export const validateForm: ActionReducer<FormState<AddressType>> = createFormStateReducerWithUpdate<AddressType>(
  updateGroup<AddressType>(
    {
      type: validate(required),
      description: validate(required),
    }
  )
);

export function reducer(
  state: FormGroupState<AddressType> = INITIAL_STATE,
  action: Action
): FormGroupState<AddressType> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }
  if (action.type === addressTypesActions.loadAddressTypeSuccess.type) {
    const item: AddressType = <AddressType>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, item);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<AddressType>) => state;

