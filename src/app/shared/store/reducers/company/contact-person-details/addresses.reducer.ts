/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Address } from '@app/shared/models/address/address.model';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';
import { createReducer, on } from '@ngrx/store';

export interface State {
  loading: boolean;
  loaded: boolean;
  failed: boolean;
  data: Array<Address>;
}

export const INITIAL_STATE: State = {
  loading: false,
  loaded: false,
  failed: false,
  data: [],
};
export const contactPersonAddressesReducer = createReducer(
  INITIAL_STATE,
  on(contactPersonActions.loadContactPersonDetailAddresses, (state: any, action: any) => {
    return {
      ...state,
      loading: true,
      loaded: false,
      failed: false,
      data: [],
    };
  }),
  on(contactPersonActions.loadContactPersonDetailAddressesSuccess, (state: any, action: any) => {
    return {
      ...state,
      loading: false,
      loaded: true,
      failed: false,
      data: action['payload'],
    };
  }),
  on(contactPersonActions.loadContactPersonDetailAddressesFail, (state: any, action: any) => {
    return {
      ...state,
      loaded: false,
      loading: false,
      failed: true,
      data: [],
    };
  })
);
export function reducer(state = INITIAL_STATE, action: any): State {
  return contactPersonAddressesReducer(state, action);
}

export const getData = (state: State) => state.data;
export const getLoading = (state: State) => state.loading;
export const getLoaded = (state: State) => state.loaded;
export const getFailed = (state: State) => state.failed;
