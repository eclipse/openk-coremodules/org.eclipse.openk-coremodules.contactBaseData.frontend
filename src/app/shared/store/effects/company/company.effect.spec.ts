/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyApiClient } from '@pages/company/company-api-client';
import { CompanyEffect } from '@shared/store/effects/company/company.effect';
import { Subject, of, throwError } from 'rxjs';
import * as companyActions from '@shared/store/actions/company/company.action';
import { Company, CommunicationsData, Address, ContactPerson } from '@shared/models';

describe('Company Effects', () => {
  let effects: CompanyEffect;
  let actions$: Subject<any>;
  let apiClient: CompanyApiClient;
  let apiResponse: any = null;
  let store: any;

  beforeEach(() => {
    apiClient = {
      getCompanyDetails() {},
      putCompanyDetails: () => {},
      postCompanyDetails: () => {},
      getCompanyDetailsAddresses: () => {},
      getCompanyDetailsAddressesDetails: () => {},
      putAddressDetails: () => {},
      postAddressDetails: () => {},
      deleteAddress: () => {},
      putCommunicationsDataDetails: () => {},
      postCommunicationsDataDetails: () => {},
      deleteCommunicationsData: () => {},
      getContactPersons: () => {},
      getCommunicationsData: () => {},
    } as any;

    actions$ = new Subject();

    store = {
      dispatch() {},
    };

    effects = new CompanyEffect(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadCompanyDetail after getCompany', done => {
    apiResponse = new Company({ id: '1' });
    spyOn(apiClient, 'getCompanyDetails').and.returnValue(of(apiResponse));
    effects.getCompany$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.loadCompanyDetailSuccess({ payload: new Company({ id: '1' }) }));
    });
    done();
    actions$.next(companyActions.loadCompanyDetail({ payload: '1' }));
  });

  it('should equal loadCompanyDetailFail after getCompany', done => {
    spyOn(apiClient, 'getCompanyDetails').and.returnValue(throwError('error'));
    effects.getCompany$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.loadCompanyDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.loadCompanyDetail({ payload: '1' }));
  });

  it('should equal putCompanyDetails after persistCompany with given contactId', done => {
    apiResponse = new Company({ contactId: '1' });
    spyOn(apiClient, 'putCompanyDetails').and.returnValue(of(apiResponse));
    effects.persistCompany$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCompanyDetailSuccess({ payload: new Company({ contactId: '1' }) }));
    });
    done();
    actions$.next(companyActions.persistCompanyDetail({ payload: new Company({ contactId: '1' }) }));
  });

  it('should equal postCompanyDetails after persistCompany a new company', done => {
    apiResponse = new Company();
    spyOn(apiClient, 'postCompanyDetails').and.returnValue(of(apiResponse));
    effects.persistCompany$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCompanyDetailSuccess({ payload: new Company() }));
    });
    done();
    actions$.next(companyActions.persistCompanyDetail({ payload: new Company() }));
  });

  it('should equal persistCompanyDetailFail after putCompanyDetails', done => {
    spyOn(apiClient, 'putCompanyDetails').and.returnValue(throwError('error'));
    effects.persistCompany$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCompanyDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.persistCompanyDetail({ payload: new Company({ contactId: '1' }) }));
  });

  it('should equal persistCompanyDetailFail after postCompanyDetails', done => {
    spyOn(apiClient, 'postCompanyDetails').and.returnValue(throwError('error'));
    effects.persistCompany$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCompanyDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.persistCompanyDetail({ payload: new Company() }));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail with given contactId', done => {
    apiResponse = new CommunicationsData({ contactId: '1', id: '1' });
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCommunicationsDataDetailSuccess({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
    });
    done();
    actions$.next(companyActions.persistCommunicationsDataDetail({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail a new internal person', done => {
    apiResponse = new CommunicationsData();
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCommunicationsDataDetailSuccess({ payload: new CommunicationsData() }));
    });
    done();
    actions$.next(companyActions.persistCommunicationsDataDetail({ payload: new CommunicationsData() }));
  });

  it('should equal persistCommunicationsDataDetailFail after putCommunicationsDataDetails', done => {
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCommunicationsDataDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.persistCommunicationsDataDetail({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistCommunicationsDataDetailFail after postCommunicationsDataDetails', done => {
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistCommunicationsDataDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.persistCommunicationsDataDetail({ payload: new CommunicationsData() }));
  });

  it('should equal deleteCommunicationsDataSuccess after deleteCommunicationsData', done => {
    apiResponse = new CommunicationsData({ contactId: '1', id: '1' });
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(of(apiResponse));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.deleteCommunicationsDataSuccess());
    });
    done();
    actions$.next(companyActions.deleteCommunicationsData({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal deleteCommunicationsDataFail after deleteCommunicationsData', done => {
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(throwError('error'));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.deleteCommunicationsDataFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.deleteCommunicationsData({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail with given contactId', done => {
    apiResponse = new Address({ contactId: '1', id: '1' });
    spyOn(apiClient, 'putAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistAddressDetailSuccess({ payload: new Address({ contactId: '1', id: '1' }) }));
    });
    done();
    actions$.next(companyActions.persistAddressDetail({ payload: new Address({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail a new company', done => {
    apiResponse = new Address();
    spyOn(apiClient, 'postAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistAddressDetailSuccess({ payload: new Address() }));
    });
    done();
    actions$.next(companyActions.persistAddressDetail({ payload: new Address() }));
  });

  it('should equal persistAddressDetailFail after putAddressDetails', done => {
    spyOn(apiClient, 'putAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistAddressDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.persistAddressDetail({ payload: new Address({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistAddressDetailFail after postAddressDetails', done => {
    spyOn(apiClient, 'postAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.persistAddressDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.persistAddressDetail({ payload: new Address() }));
  });

  it('should equal deleteAddressSuccess after deleteAddress', done => {
    apiResponse = new Address({ contactId: '1', id: '1' });
    spyOn(apiClient, 'deleteAddress').and.returnValue(of(apiResponse));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.deleteAddressSuccess());
    });
    done();
    actions$.next(companyActions.deleteAddress({ payload: new Address({ contactId: '1', id: '1' }) }));
  });

  it('should equal deleteAddressFail after deleteAddress', done => {
    spyOn(apiClient, 'deleteAddress').and.returnValue(throwError('error'));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.deleteAddressFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.deleteAddress({ payload: new Address({ contactId: '1', id: '1' }) }));
  });

  it('should equal loadCompanyDetailContactPersonsSuccess after getContactPersons', done => {
    apiResponse = [new ContactPerson({ id: '1' })];
    spyOn(apiClient, 'getContactPersons').and.returnValue(of(apiResponse));
    effects.getCompanyContactPersons$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.loadCompanyDetailContactPersonsSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(companyActions.loadCompanyDetailContactPersons({ payload: '1' }));
  });

  it('should equal loadCompanyDetailContactPersonsFail after getContactPersons Error', done => {
    spyOn(apiClient, 'getContactPersons').and.returnValue(throwError('error'));
    effects.getCompanyContactPersons$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.loadCompanyDetailContactPersonsFail({ payload: 'error' }));
    });
    done();
    actions$.next(companyActions.loadCompanyDetailContactPersons({ payload: '1' }));
  });

  it('should equal loadCompanyDetailCommunicationsDataSuccess after getCommunicationsData', done => {
    apiResponse = [new CommunicationsData({ id: '1' })];
    spyOn(apiClient, 'getCommunicationsData').and.returnValue(of(apiResponse));
    effects.getCompanyCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(companyActions.loadCompanyDetailCommunicationsDataSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(companyActions.loadCompanyDetailCommunicationsData({ payload: '1' }));
  });
});
