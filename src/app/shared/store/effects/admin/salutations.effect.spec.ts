/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { take } from 'rxjs/operators';
import { SalutationsEffects } from '@shared/store/effects/admin/salutations.effect';
import { Subject, of, throwError } from 'rxjs';
import { Salutation } from '@shared/models';
import * as salutationsActions from '@shared/store/actions/admin/salutations.action';
import { SalutationsApiClient } from '@pages/admin/salutations/salutations-api-client';
import { Store } from '@ngrx/store';

describe('Salutations Effects', () => {

  let effects: SalutationsEffects;
  let actions$: Subject<any>;
  let apiClient: SalutationsApiClient;
  let store: Store<any>;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getSalutations() {},
      getSalutationDetails() {},
      putSalutation() {},
      postSalutation() {},
      deleteSalutation() {}
    } as any;
    store = {
      dispatch() {}
    } as any;
    actions$ = new Subject();
    effects = new SalutationsEffects(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadSalutationsSuccess after getSalutations', (done) => {
    apiResponse =  [new Salutation({id: "1"})];
    spyOn(apiClient, 'getSalutations').and.returnValue(of(apiResponse));
    effects.getSalutations$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(salutationsActions.loadSalutationsSuccess({payload: apiResponse}));
    });
    done();
    actions$.next(salutationsActions.loadSalutations());
  });

  it('should equal loadSalutationsFail after getSalutations Error', (done) => {
    spyOn(apiClient, 'getSalutations').and.returnValue(throwError('x'));
    effects.getSalutations$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(salutationsActions.loadSalutationsFail({payload: 'x'}));
    });
    done();
    actions$.next(salutationsActions.loadSalutations());
  });

  it('should equal loadSalutationSuccess after getSalutation', (done) => {
    apiResponse =  new Salutation({id: "1"});
    spyOn(apiClient, 'getSalutationDetails').and.returnValue(of(apiResponse));
    effects.getSalutation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(salutationsActions.loadSalutationSuccess({payload: apiResponse}));
    });
    done();
    actions$.next(salutationsActions.loadSalutation({payload: "1"}));
  });

  it('should equal loadSalutationFail after getSalutation Error', (done) => {
    spyOn(apiClient, 'getSalutationDetails').and.returnValue(throwError('x'));
    effects.getSalutation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(salutationsActions.loadSalutationFail({payload: 'x'}));
    });
    done();
    actions$.next(salutationsActions.loadSalutation({payload: "1"}));
  });

  it('should equal saveSalutationSuccess after saveSalutation', (done) => {
    apiResponse =  new Salutation({id: "1"});
    spyOn(apiClient, 'putSalutation').and.returnValue(of(apiResponse));
    effects.saveSalutation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(salutationsActions.saveSalutationSuccess({payload: new Salutation({id: "1"})}));
    });
    done();
    actions$.next(salutationsActions.saveSalutation({payload: new Salutation({id: "1"})}));
  });

  it('should equal saveSalutationFail after saveSalutation Error', (done) => {
    spyOn(apiClient, 'putSalutation').and.returnValue(throwError('x'));
    effects.saveSalutation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(salutationsActions.saveSalutationFail({payload: 'x'}));
    });
    done();
    actions$.next(salutationsActions.saveSalutation({payload: new Salutation({id: "1"})}));
  });

  it('should equal deleteSalutationSuccess after deleteSalutation', (done) => {
    apiResponse =  new Salutation({id: "1"});
    spyOn(apiClient, 'deleteSalutation').and.returnValue(of(null));
    effects.deleteSalutation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(salutationsActions.deleteSalutationSuccess());
    });
    done();
    actions$.next(salutationsActions.deleteSalutation({payload: "1"}));
  });

});
