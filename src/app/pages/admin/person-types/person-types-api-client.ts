import { PersonTypesService } from '@pages/admin/person-types/person-types.service';
import { Injectable } from '@angular/core';
import { HttpService, GET, Adapter, DefaultHeaders, Path, PUT, Body, POST, DELETE } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { PersonType } from '@shared/models';

@Injectable()
@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
})
export class PersonTypesApiClient extends HttpService {

  @GET('/personTypes')
  @Adapter(PersonTypesService.gridAdapter)
  public getPersonTypes(): Observable<PersonType[]> {
    return null;
  }

  @GET('/personTypes/{id}')
  @Adapter(PersonTypesService.itemAdapter)
  public getPersonTypeDetails(@Path('id') id: string): Observable<PersonType> {
    return null;
  }

  @PUT('/personTypes/{id}')
  @Adapter(PersonTypesService.itemAdapter)
  public putPersonType(@Path('id') id: string, @Body() item: PersonType): Observable<PersonType> {
    return null;
  }

  @POST('/personTypes')
  @Adapter(PersonTypesService.itemAdapter)
  public postPersonType(@Body() newPersonType: PersonType): Observable<PersonType> {
    return null;
  }

  @DELETE('/personTypes/{id}')
  @Adapter(PersonTypesService.itemAdapter)
  public deletePersonType(@Path('id') id: string): Observable<void> {
    return null;
  }

}
