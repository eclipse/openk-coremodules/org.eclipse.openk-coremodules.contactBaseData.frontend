/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UtilService } from '@shared/utility/utility.service';
import { Injectable } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import * as store from '@shared/store';
import * as addressTypesActions from '@shared/store/actions/admin/address-types.action';
import * as addressTypesFormReducer from '@shared/store/reducers/admin/address-types-details-form.reducer';
import { AddressType } from '@shared/models';
import { FormGroupState, SetValueAction, ResetAction, MarkAsTouchedAction } from 'ngrx-forms';
import { ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { takeUntil, take } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { ILoadAddressTypesSuccess } from '@shared/store/actions/admin/address-types.action';

@Injectable()
export class AddressTypesSandbox extends BaseSandbox {
  public addressTypes$ = this.appState$.select(store.getAddressTypesData);
  public addressTypesLoading$ = this.appState$.select(store.getAddressTypesLoading);
  public formState$ = this.appState$.select(store.getAddressTypesDetails);
  public currentFormState: FormGroupState<AddressType>;
  public displayForm: boolean = false;

  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    protected router: Router,
    protected utilService: UtilService,
    protected modalService: NgbModal
  ) {
    super(appState$);
  }

  public setDisplayForm(): void {
    this.clear();
    this.appState$.dispatch(new MarkAsTouchedAction(addressTypesFormReducer.FORM_ID));
    this.displayForm = true;
  }

  public loadAddressTypes(): Observable<ILoadAddressTypesSuccess> {
    this.appState$.dispatch(addressTypesActions.loadAddressTypes());
    return this.actionsSubject.pipe(ofType(addressTypesActions.loadAddressTypesSuccess), take(1));
  }

  public loadAddressType(id: string): void {
    this.appState$.dispatch(addressTypesActions.loadAddressType({ payload: id }));
  }

  public deleteAddressType(id: string): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(addressTypesActions.deleteAddressType({ payload: id }));
        this.clear();
      },
      () => {}
    );
  }

  public cancel(): void {
    if (!this.currentFormState.isPristine) {
      const modalRef = this.modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.edit';
      modalRef.componentInstance.body = 'ConfirmDialog.Content';
      modalRef.result.then(
        () => {
          this.clear();
        },
        () => {}
      );
    } else {
      this.clear();
    }
  }

  clear(): void {
    this.appState$.dispatch(new SetValueAction(addressTypesFormReducer.FORM_ID, addressTypesFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(addressTypesFormReducer.FORM_ID));
    this.displayForm = false;
  }

  public saveAddressType(): void {
    if (this.currentFormState.isValid) {
      this.appState$.dispatch(
        addressTypesActions.saveAddressType({
          payload: new AddressType(this.currentFormState.value),
        })
      );
      this.actionsSubject.pipe(ofType(addressTypesActions.saveAddressTypeSuccess), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.clear();
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldError', 'alert');
    }
  }

  public registerEvents(): void {
    this.formState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<AddressType>) => (this.currentFormState = formState));
  }
}
