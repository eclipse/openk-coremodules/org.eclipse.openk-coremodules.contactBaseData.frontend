/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddressTypesSandbox } from '@pages/admin/address-types//address-types.sandbox';
import { Store, ActionsSubject } from '@ngrx/store';
import { State } from '@shared/store';
import { UtilService } from '@shared/utility/utility.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import * as addressTypesActions from '@shared/store/actions/admin/address-types.action';
import { AddressType } from '@shared/models';

describe('AddressTypesSandbox', () => {
  let service: AddressTypesSandbox;
  let appState: Store<State>;
  let actionSubject: ActionsSubject;
  let utilService: UtilService;
  let router: Router;
  let modalService: NgbModal;

  beforeEach(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;
    utilService = { displayNotification() {} } as any;
    router = { navigateByUrl() {} } as any;
    modalService = { open() {} } as any;
    spyOn(appState, 'dispatch').and.callFake(() => {});

    service = new AddressTypesSandbox(appState, actionSubject, router, utilService, modalService);
  });

  it('should create AddressTypesSandbox service', () => {
    expect(service).toBeTruthy();
  });

  it('should set DisplayForm property', () => {
    service.setDisplayForm();
    expect(service.displayForm).toBeTruthy();
  });

  it('should dispatch loadAddressTypes Action via loadAddressTypes()', () => {
    service.loadAddressTypes();
    expect(appState.dispatch).toHaveBeenCalledWith(addressTypesActions.loadAddressTypes());
  });

  it('should dispatch loadAddressType Action via loadAddressType(id)', () => {
    service.loadAddressType('x');
    expect(appState.dispatch).toHaveBeenCalledWith(addressTypesActions.loadAddressType({ payload: 'x' }));
  });

  it('should call dispatch for saving an address type', () => {
    service.currentFormState = {isValid: true} as any;
    service.saveAddressType();
    expect(appState.dispatch).toHaveBeenCalledWith(addressTypesActions.saveAddressType({
        payload: new AddressType()
      })
    );
  });

  it('should clear form via clear()', () => {
    service['clear']();
    expect(service.displayForm).toBe(false);
  });

  it('should open modal before deleting a AddressType', () => {
    spyOn(service['modalService'], 'open')
      .and.returnValue({componentInstance: {title: ''}, result: {then: () => of(true)}} as any);
    service.deleteAddressType('x');
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should clear form state when current change is canceled and form state is pristine', () => {
    let spy = spyOn(service,'clear');
    service.currentFormState = {isPristine: true} as any;
    service.cancel();
    expect(spy).toHaveBeenCalled();
  });

  it('should open modal when current change is canceled and form state is not pristine', () => {
    spyOn(service['modalService'], 'open')
      .and.returnValue({componentInstance: {title: ''}, result: {then: () => of(true)}} as any);
    service.currentFormState = {isPristine: false} as any;
    service.cancel();
    expect(modalService.open).toHaveBeenCalled();
  });
});
