 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SalutationsService } from '@pages/admin/salutations/salutations.service';
import { Injectable } from '@angular/core';
import { HttpService, GET, Adapter, DefaultHeaders, Path, PUT, Body, POST, DELETE } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { Salutation } from '@shared/models';

@Injectable()
@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
})
export class SalutationsApiClient extends HttpService {

  @GET('/salutations')
  @Adapter(SalutationsService.gridAdapter)
  public getSalutations(): Observable<Array<Salutation>> {
    return null;
  }

  @GET('/salutations/{id}')
  @Adapter(SalutationsService.salutationAdapter)
  public getSalutationDetails(@Path('id') id: string): Observable<Salutation> {
    return null;
  }

  @PUT('/salutations/{id}')
  @Adapter(SalutationsService.salutationAdapter)
  public putSalutation(@Path('id') id: string, @Body() salutation: Salutation): Observable<Salutation> {
    return null;
  }

  @POST('/salutations')
  @Adapter(SalutationsService.salutationAdapter)
  public postSalutation(@Body() newSalutation: Salutation): Observable<Salutation> {
    return null;
  }

  @DELETE('/salutations/{id}')
  @Adapter(SalutationsService.salutationAdapter)
  public deleteSalutation(@Path('id') id: string): Observable<void> {
    return null;
  }

}
