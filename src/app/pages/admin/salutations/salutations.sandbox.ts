/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UtilService } from '@shared/utility/utility.service';
import { Injectable } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import * as store from '@shared/store';
import * as salutationsActions from '@shared/store/actions/admin/salutations.action';
import * as salutationFormReducer from '@shared/store/reducers/admin/salutations-details-form.reducer';
import { Salutation } from '@shared/models';
import { FormGroupState, SetValueAction, ResetAction, MarkAsTouchedAction } from 'ngrx-forms';
import { ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { takeUntil, take } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { ILoadSalutationsSuccess } from '@shared/store/actions/admin/salutations.action';

@Injectable()
export class SalutationsSandbox extends BaseSandbox {
  public salutations$ = this.appState$.select(store.getSalutationsData);
  public salutationsLoading$ = this.appState$.select(store.getSalutationsLoading);
  public formState$ = this.appState$.select(store.getSalutationDetails);

  public currentFormState: FormGroupState<Salutation>;
  public displayForm: boolean = false;

  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    protected router: Router,
    protected utilService: UtilService,
    protected modalService: NgbModal
  ) {
    super(appState$);
  }

  public setDisplayForm(): void {
    this.clear();
    this.appState$.dispatch(new MarkAsTouchedAction(salutationFormReducer.FORM_ID));
    this.displayForm = true;
  }

  public loadSalutations(): Observable<ILoadSalutationsSuccess> {
    this.appState$.dispatch(salutationsActions.loadSalutations());
    return this.actionsSubject.pipe(ofType(salutationsActions.loadSalutationsSuccess), take(1));
  }

  public loadSalutation(id: string): void {
    this.appState$.dispatch(salutationsActions.loadSalutation({ payload: id }));
  }

  public deleteSalutation(id: string): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(salutationsActions.deleteSalutation({ payload: id }));
        this.clear();
      },
      () => {}
    );
  }

  public cancel(): void {
    if (!this.currentFormState.isPristine) {
      const modalRef = this.modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.edit';
      modalRef.componentInstance.body = 'ConfirmDialog.Content';
      modalRef.result.then(
        () => {
          this.clear();
        },
        () => {}
      );
    } else {
      this.clear();
    }
  }

  clear(): void {
    this.appState$.dispatch(new SetValueAction(salutationFormReducer.FORM_ID, salutationFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(salutationFormReducer.FORM_ID));
    this.displayForm = false;
  }

  public saveSalutation(): void {
    if (this.currentFormState.isValid) {
      this.appState$.dispatch(
        salutationsActions.saveSalutation({
          payload: new Salutation(this.currentFormState.value),
        })
      );
      this.actionsSubject.pipe(ofType(salutationsActions.saveSalutationSuccess), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.clear();
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldError', 'alert');
    }
  }

  public registerEvents(): void {
    this.formState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<Salutation>) => (this.currentFormState = formState));
  }
}
