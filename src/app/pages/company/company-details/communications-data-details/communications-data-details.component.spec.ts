/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { CompanyCommunicationsDataDetailsComponent } from '@pages/company/company-details/communications-data-details/communications-data-details.component';

describe('CompanyCommunicationsDataDetailsComponent', () => {
  let component: CompanyCommunicationsDataDetailsComponent;
  let companySandbox: any;
  let communicationTypesSandbox: any;

  beforeEach(async(() => {
    companySandbox = {
      registerCommunicationsDataEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      clearCommunicationsData() {},
    } as any;

    communicationTypesSandbox = {
    } as any;
  }));

  beforeEach(() => {
    component = new CompanyCommunicationsDataDetailsComponent(companySandbox, communicationTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerCommunicationsDataEvents and clearCommunicationsData onInit', () => {
    const spy1 = spyOn(companySandbox, 'registerCommunicationsDataEvents');
    const spy2 = spyOn(companySandbox, 'clearCommunicationsData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
