/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPerson, InternalPerson } from '@shared/models';
import { Address, CommunicationsData, KeycloakUser, LdapUser } from '@shared/models';
import { Injectable } from '@angular/core';
import { PageModel } from '@shared/models/page/page.model';

/**
 * Used to retrieve persons
 *
 * @export
 * @class PersonsService
 */
@Injectable()
export class PersonsService {
  /**
   * Transforms external person details recieved from the API into instance of 'ExternalPerson'
   *
   * @param response
   */
  static externalPersonDetailsAdapter(response: any): ExternalPerson {
    return new ExternalPerson(response);
  }

  /**
   * Transforms person's details addresses received from the API into instance of 'address[]'
   *
   * @param response
   */
  static addressesAdapter(response: any): Array<Address> {
    return response.map(address => new Address(address));
  }

  /**
   * Transforms person's details addresses details recieved from the API into instance of 'address'
   *
   * @param response
   */
  static addressesDetailsAdapter(response: any): Address {
    return new Address(response);
  }

  /**
   * Transforms internal person details recieved from the API into instance of 'InternalPerson'
   *
   * @param response
   */
  static internalPersonDetailsAdapter(response: any): InternalPerson {
    return new InternalPerson(response);
  }

  /**
   * Transforms grid data received from the API into array of PageModel instances
   *
   * @param response
   */
  static internalPersonsAdapter(response: PageModel<InternalPerson>): Array<InternalPerson> {
    return response != null ? response.content : null;
  }

  /**
   * Transforms communications data received from the API into instance of 'CommunicationsData[]'
   *
   * @param response
   */
  static communicationsDataAdapter(response: any): Array<CommunicationsData> {
    return response.map(communicationsData => new CommunicationsData(communicationsData));
  }

  /**
   * Transforms communications data details recieved from the API into instance of 'CommunicationsData'
   *
   * @param response
   */
  static communicationsDataDetailsAdapter(response: any): CommunicationsData {
    return new CommunicationsData(response);
  }

  /**
   * Transforms keycloak user received from the API into instance of 'KeycloakUser[]'
   *
   * @param response
   */
  static keycloakUsersAdapter(response: any): Array<KeycloakUser> {
    return response.map(user => new KeycloakUser(user));
  }

  /**
   * Transforms ldap user received from the API into instance of 'LdapUser[]'
   *
   * @param response
   */
  static ldapUsersAdapter(response: any): Array<LdapUser> {
    return response.map(user => new LdapUser(user));
  }
}
