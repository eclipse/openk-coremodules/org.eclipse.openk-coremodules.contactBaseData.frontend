/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { InternalPersonDetailsComponent } from '@pages/persons/internal-person/internal-person-details/internal-person-details.component';

describe('InternalPersonDetailsComponent', () => {
  let component: InternalPersonDetailsComponent;
  let internalPersonSandbox: any;
  let userModuleAssignmentSandBox: any;
  let translate: any;
  let salutationsSandbox: any;
  let personTypesSandbox: any;

  beforeEach(() => {
    internalPersonSandbox = {
      registerAddressEvents() {},
      registerInternalPersonEvents() {},
      endSubscriptions() {},
      loadCommunicationsDataDetails() {},
      loadInternalPersonDetailsAddressDetails() {},
    } as any;

    salutationsSandbox = {} as any;

    personTypesSandbox = {} as any;

    translate = {
      instant() {},
    } as any;

    component = new InternalPersonDetailsComponent(internalPersonSandbox, userModuleAssignmentSandBox, salutationsSandbox, personTypesSandbox, translate);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should loadSalutations on init', () => {
    const spy = spyOn(internalPersonSandbox, 'registerInternalPersonEvents');
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should end all subscriptions for this sandbox onDestroy', () => {
    const spy = spyOn(internalPersonSandbox, 'endSubscriptions');
    component.ngOnDestroy();

    expect(spy).toHaveBeenCalled();
  });

  it('should call translate instant if no communications data id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadCommunicationsDataDetail(null);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.NewCommunicationsData');
  });

  it('should call translate instant if there is an communications data id', () => {
    const id = 'id';
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(internalPersonSandbox, 'loadCommunicationsDataDetails');
    component.loadCommunicationsDataDetail(id);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.EditCommunicationsData');
    expect(spy1).toHaveBeenCalledWith(id);
  });

  it('should call translate instant if there is an communications data id', () => {
    component.internalPersonSandBox.isCommunicationsDataDetailViewVisible = false;
    component.loadCommunicationsDataDetail(null);
    expect(component.internalPersonSandBox.isCommunicationsDataDetailViewVisible).toBeTruthy();
  });

  it('should call translate instant if no id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadAddressDetail(null);

    expect(spy).toHaveBeenCalledWith('Address.NewAddress');
  });

  it('should call translate instant if there is id', () => {
    const id = 'id';
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(internalPersonSandbox, 'loadInternalPersonDetailsAddressDetails');
    component.loadAddressDetail(id);

    expect(spy).toHaveBeenCalledWith('Address.EditAddress');
    expect(spy1).toHaveBeenCalledWith(id);
  });

  it('should call translate instant if there is id', () => {
    component.internalPersonSandBox.isAddressDataDetailViewVisible = false;
    component.loadAddressDetail(null);
    expect(component.internalPersonSandBox.isAddressDataDetailViewVisible).toBeTruthy();
  });

  it('should set expandableVisible to "true" if internalPersonContactId is defined', () => {
    component.internalPersonSandBox.internalPersonContactId = 'test';
    (component as any)._initExpandableState();
    expect(component.isExpandableVisible).toBeTruthy();
    expect(component.internalPersonSandBox.isAddressDataDetailViewVisible).toBeFalsy();
    expect(component.internalPersonSandBox.isCommunicationsDataDetailViewVisible).toBeFalsy();
  });
});
