# Contributing to Eclipse openK User Modules

Thanks for your interest in this project.

## Project description

The project starts with the module Feed-In Management ("Last- und
Einspeisemanagement") based on the German Electricity Feed-In Act. The future
demand for electric energy is to be adapted to the supply and availability of
renewable energy. Based on specific rules and parameters the Feed-In Management
calculates a blueprint for electric switching stations and power plants based on
renewable energy. Implementing the calculation input data is needed from
electric networks, switching stations, power plants, electric demands and
feed-in capabilities.

* https://projects.eclipse.org/projects/technology.openk-usermodules

## Developer resources

Information regarding source code management, builds, coding standards, and
more.

* https://projects.eclipse.org/projects/technology.openk-usermodules/developer

The project maintains the following source code repositories

* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/openk-usermodules
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.contactBaseData.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.contactBaseData.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.centralService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.homeService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.docu
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.frontend

This project uses Bugzilla to track ongoing development and issues.

* Search for issues: https://bugs.eclipse.org/bugs/buglist.cgi?product=openK
   User Modules
* Create a new report:
   https://bugs.eclipse.org/bugs/enter_bug.cgi?product=openK User Modules

Be sure to search for existing bugs before you create another one. Remember that
contributions are always welcome!

## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* http://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the project developers via the project's "dev" list.

* https://accounts.eclipse.org/mailing-list/openk-usermodules-dev
